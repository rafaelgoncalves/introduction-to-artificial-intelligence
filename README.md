# Introduction to artificial intelligence

Projects and exercises for course EA072 (2s2020) on artificial intelligence for industrial applications @ Unicamp

## Computational Exercises (ECp)

### ECp1 - Artificial Neural Networks (ANN)

  1. Perceptron (Linear Classifier)
  1. ELM (Extreme Learn Machine)
  1. MLP (Multi-Layer Perceptron)
  1. CNN (Convolutional Neural Network
  1. AE (Auto Encoder)

### ECp2 - Artificial Neural Networks (ANN)

  1. Interpretability (LRP and activation)
  1. Reinforcement Learning (Q-learning)
  1. GAN (Genrative Adversarial Networks)
  1. Word2vec and t-SNE (NLP and visualization of vector spaces)

### ECp3 - Misc

  1. Evolutionary algorithm
  1. Baysian analysis
  1. Decision trees

## Conceptual Exercises (ECc)

### ECc1 - Computational Inteligence
  1. Least Squares versus Gradient Descent in ANN
  1. LASSO, Ridge Regression and Elastic Net
  1. Variance of weights matrix in MLP
  1. Mutation operator for real variable (Genetic Algorithm)
  1. Fitness function (Evolutionary Algorithm)
  1. Simulated annealing, Ant-colony optimization and evolutionary algorithm
  1. Solution spaces
  1. Fuzzy systems

### ECc2 - Classical AI
  1. Logic
  1. A* search
  1. Decision trees
  1. Game theory
  1. Baysian analysis
