import numpy as np
from utils import plot_confusion_matrix
from sklearn import metrics
import matplotlib.pyplot as plt
import scipy.io
from matplotlib import cm
from matplotlib.colors import ListedColormap, LinearSegmentedColormap

print("Question 1 - MNIST evaluation using multiclass perceptron\n")

# Deterministic pseudo-randomness
np.random.seed(0)

# Load data
train = scipy.io.loadmat('aux/data.mat')
test = scipy.io.loadmat('aux/test.mat')

N, m = train['X'].shape
X = np.hstack((np.ones((N, 1)), train['X']))

Nt, mt = test['Xt'].shape
Xt = np.hstack((np.ones((Nt, 1)), test['Xt']))
St = test['St']

# Scramble indexes
new_order = np.random.permutation(N)
X = X[new_order,:]
S = train['S']
S = S[new_order, :]

# Create validation set
tr_pct = 0.8
div_idx = int(tr_pct*N)
Xtr = X[1:div_idx, :]
Xv = X[div_idx:, :]
Str = S[1:div_idx, :]
Sv = S[div_idx:, :]

# Auxiliary functions
MSE = lambda y, y_hat: np.mean(np.square(y - y_hat))
MMQ = lambda X, y: np.dot(np.dot(np.linalg.inv(np.dot(X.T, X) + c*np.eye(m+1)), X.T), y)

# Choose best value for regularization c
v_c = []
v_mse = []
v_tx_correct = []
tx_correct_max = 0

print("...calculating best c value")
for p in range(-10,13,2):
    c = 2**p
    w = MMQ(Xtr, Str)
    S_hat = np.dot(Xv,w)
    correct = 0
    for i, e in enumerate(Xv):
        vmax = np.max(S_hat[i,:])
        idx_max = np.argmax(S_hat[i,:])
        if Sv[i,idx_max] > 0.5:
            correct += 1
    tx_correct = correct/(0.2*N)
    loss = MSE(Sv, S_hat)
    v_c.append(c)
    v_mse.append(loss)
    v_tx_correct.append(tx_correct)

    # DEBUG
    # print(c, correct/(0.2*N), loss)

c_min_mse = v_c[np.argmin(v_mse)]
min_mse_mse = v_mse[np.argmin(v_mse)]
min_mse_acc = v_tx_correct[np.argmin(v_mse)]
c_max_acc = v_c[np.argmax(v_tx_correct)]
max_acc_mse = v_mse[np.argmax(v_tx_correct)]
max_acc_acc = v_tx_correct[np.argmax(v_tx_correct)]

# Plot search of best value for regularization c
print("...saving images")
fig1 = plt.figure()
ax1 = fig1.add_subplot(111)
ax1.grid(True)
ax1.semilogx(v_c, v_mse)
ax1.set_xlabel("c")
ax1.set_ylabel("MSE")
fig1.savefig('out/1-loss.png')

fig2 = plt.figure()
ax1 = fig2.add_subplot(111)
ax1.grid(True)
ax1.semilogx(v_c, v_tx_correct)
ax1.set_xlabel("c")
ax1.set_ylabel("Accuracy")
fig2.savefig('out/1-acc.png')

# Refining stage
v_c = []
v_mse = []
v_tx_correct = []
tx_correct_max = 0

print("...initializing refining stage")
for p in np.linspace(int(np.log2(min(c_min_mse, c_max_acc)/2)), int(np.log2(max(c_min_mse, c_max_acc)*2)), 20):
    c = 2**p
    w = MMQ(Xtr, Str)
    S_hat = np.dot(Xv,w)
    correct = 0
    for i, e in enumerate(Xv):
        vmax = np.max(S_hat[i,:])
        idx_max = np.argmax(S_hat[i,:])
        if Sv[i,idx_max] > 0.5:
            correct += 1
    tx_correct = correct/(0.2*N)
    loss = MSE(Sv, S_hat)
    v_c.append(c)
    v_mse.append(loss)
    v_tx_correct.append(tx_correct)

    #DEBUG
    # print(c, tx_correct, loss)

c_min_mse_r = v_c[np.argmin(v_mse)]
c_max_acc_r = v_c[np.argmax(v_tx_correct)]

min_mse_mse_r = v_mse[np.argmin(v_mse)]
min_mse_acc_r = v_tx_correct[np.argmin(v_mse)]
max_acc_mse_r = v_mse[np.argmax(v_tx_correct)]
max_acc_acc_r = v_tx_correct[np.argmax(v_tx_correct)]

# Plot search of best value for regularization c
print("...saving images")
fig3 = plt.figure()
ax1 = fig3.add_subplot(111)
ax1.grid(True)
ax1.semilogx(v_c, v_mse)
ax1.set_xlabel("c")
ax1.set_ylabel("MSE")
fig3.savefig('out/1-loss_ref.png')

fig4 = plt.figure()
ax1 = fig4.add_subplot(111)
ax1.grid(True)
ax1.semilogx(v_c, v_tx_correct)
ax1.set_xlabel("c")
ax1.set_ylabel("Accuracy")
fig4.savefig('out/1-acc_ref.png')

# Final evaluation
print("...training net")
c = min((c_max_acc, c_max_acc_r))
w = MMQ(X, S)

print("...final evaluation")
S_hat = np.dot(Xt,w)
loss = MSE(S_hat, St)
correct = 0
for i, e in enumerate(Xt):
    vmax = np.max(S_hat[i,:])
    idx_max = np.argmax(S_hat[i,:])
    if St[i,idx_max] > 0.5:
        correct += 1
acc = correct/Nt


# Confusion matrix

classes_ = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0"]
classes__ = list(range(1, 10))
classes__.append(0)

def confusion(y_hat, y, n_classes, classes=classes__):
    y_hat_c = np.argmax(y_hat, 1)
    y_c = np.argmax(y, 1)
    ans = np.zeros((n_classes, n_classes))
    for i, (e, f) in enumerate(zip(y_c, y_hat_c)):
        ans[classes.index(e)][int(f)] += 1
    return ans

print("...calculating confusion matrix")
cm = confusion(S_hat, St, 10)
np.savetxt("out/1-confusion.txt", cm, delimiter=",")
# DEBUG
# print(cm)

print("...saving cm image")
y_hat_c = np.argmax(S_hat, 1)
y_c = np.argmax(St, 1)
plot_confusion_matrix(y_c, y_hat_c, classes=classes_,title='Confusion matrix', save_path="out/1-cm.png")

def print_colormaps(w):
    fig = plt.figure()
    for i in range(0,np.shape(w)[1]):
        k = 1
        img = np.empty((28,28))
        for l in range(28):
            # for c in range(27,-1,-1):
            for c in range(28):
                img[l,c] = w[k,i]
                k += 1
        ax = fig.add_subplot(3,4,i+1)
        ax.set_axis_off()
        ax.set_title("#"+classes_[i])
        ax = plt.imshow(img.T)
    fig.tight_layout()
    fig.savefig('out/1-cm_figures.png')
    return ax

#DEBUG
# print(np.shape(w))

print("...saving colormaps")
print_colormaps(w)

print("...saving synaptic weights")
# Save synaptic weights vector
np.savetxt("out/1-w186062.txt", w, delimiter=",")

print("...saving false examples")
examples = 0
fig = plt.figure(figsize=(10,4))
for i, (e, f) in enumerate(zip(S_hat, St)):
    if np.argmax(e) != np.argmax(f):
        examples += 1
        k = 1
        img = np.empty((28,28))
        for l in range(28):
            for c in range(28):
                img[l,c] = Xt[i,k]
                k += 1
        ax = fig.add_subplot(1,3,examples)
        ax.set_axis_off()
        ax.set_title("predict:"+classes_[np.argmax(e)]+" - true:" + classes_[np.argmax(f)])
        ax = plt.imshow(img.T)
    if examples >= 3:
        break
fig.tight_layout()
fig.savefig('out/1-examples.png')


print('')
print('Before refining:')
print('c_mse', c_min_mse, 'mse', min_mse_mse, 'acc', min_mse_acc)
print('c_acc', c_max_acc, 'mse', max_acc_mse, 'acc', max_acc_acc)
print('After refining:')
print('c_mse', c_min_mse_r, 'mse', min_mse_mse_r, 'acc', min_mse_acc_r)
print('c_acc', c_max_acc_r, 'mse', max_acc_mse_r, 'acc', max_acc_acc_r)
print('')
print('loss:', loss, 'acc:', acc)
