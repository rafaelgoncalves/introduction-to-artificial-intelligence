% EA072 - Intelig�ncia Artificial em Aplica��es Industriais
% Programa vinculado � Q10 do ECp3
% FEEC/Unicamp
% Prof. Fernando Jos� Von Zuben
% 02/12/2020
pkg load control;

clear all;
close all;
% Caso se queira fixar a semente do gerador pseudo-aleat�rio, use o comando
% a seguir, onde a semente � tomada como 0, mas pode ser qualquer inteiro.
% rand('state',0);
% Define o tamanho da popula��o [tam_pop], como um n�mero par
tam_pop = 100;
% Define as taxas de crossover e de muta��o
pc = 0.5;
pm = 0.7;
% Define o intervalo de excurs�o das vari�veis
v_inf = 0.0;
v_sup = 20.0;
% Define o n�mero de gera��es
n_ger = 80;
% Estruturas para preservar o melhor indiv�duo a cada gera��o e seu fitness
v_melhor_fitness = [];
v_fitness_medio = [];
v_melhor_ind = [];
% Gera a popula��o inicial respeitando o intervalo de excurs�o das vari�veis. Cada indiv�duo � uma linha da matriz [pop].
pop = v_inf+(v_sup-v_inf)*rand(tam_pop,3);
% Avalia a popula��o inicial
for i=1:tam_pop,
    kp = pop(i,1);
    kd = pop(i,2);
    ki = pop(i,3);
    S = stepinfo_EA072(G_MF(kp,kd,ki));
    [Gm,Pm,Wcg,Wcp] = margin(G_MF(kp,kd,ki));
    t1 = 0.0;t2 = 0.0;t3 = 0.0;
    if isnan(S.SettlingTime) || isnan(S.RiseTime) || isnan(S.Overshoot)
        fitness(i,1) = 0;;
    else
        if S.SettlingTime > 0.5,
            t1 = S.SettlingTime - 0.5;
        end
        if S.RiseTime > 0.04,
            t2 = S.RiseTime - 0.04;
        end
        if S.Overshoot > 0.15,
            t3 = S.Overshoot - 0.15;
        end;
        fitness(i,1) = 1/(t1+(0.5/0.04)*t2+t3+1);
    end
end
% Loop para gerar a nova popula��o at� atingir um n�mero m�ximo de gera��es
[melhor_fitness,melhor_ind] = max(fitness);
v_melhor_fitness = [v_melhor_fitness;melhor_fitness];
v_fitness_medio = [v_fitness_medio;mean(fitness)];
v_melhor_ind = [v_melhor_ind;pop(melhor_ind,:)];
v_delta = [];
T = [0:0.0005:2];
B = [0:20];
for k = 1:n_ger,
    % Em lugar da roleta, adota-se torneio de 3, pois deve-se excluir os
    % indiv�duos de fitness nulo e a implementa��o fica melhor
    candidatos = [];
    for i=1:tam_pop,
        if fitness(i,1) > 0,
            candidatos = [candidatos;i];
        end
    end
    n_tam_pop = length(candidatos);
    n_pop = [];
%    [[1:tam_pop]' pop fitness]
    for i=1:tam_pop,
        torneio = 1 + round((n_tam_pop-1)*rand(3,1));
        c_fitness(1,1) = fitness(candidatos(torneio(1,1),1),1);
        c_fitness(2,1) = fitness(candidatos(torneio(2,1),1),1);
        c_fitness(3,1) = fitness(candidatos(torneio(3,1),1),1);
        [v_max,ind_max] = max(c_fitness);
        n_pop = [n_pop;pop(candidatos(torneio(ind_max,1),1),:)];
    end
    % Aplica��o de crossover onde for escolhido
    for j=1:(tam_pop/2),
        if rand(1,1) <= pc,
            % 50% de chance de ocorrer o crossover aritm�tico
            if rand(1,1) <= 0.5,
                a = -0.1+1.2*rand(1,1);
                n_pop1 = a*n_pop(2*(j-1)+1,:)+(1-a)*n_pop(2*(j-1)+2,:);
                n_pop2 = (1-a)*n_pop(2*(j-1)+1,:)+a*n_pop(2*(j-1)+2,:);
                n_pop(2*(j-1)+1,:) = n_pop1;
                n_pop(2*(j-1)+2,:) = n_pop2;
            else
                % 50% de chance de ocorrer o crossover uniforme
                for z=1:3,
                    if rand(1,1) <= 0.5;
                        n_pop1(1,z) = n_pop(2*(j-1)+1,z);
                        n_pop2(1,z) = n_pop(2*(j-1)+2,z);
                    else
                        n_pop1(1,z) = n_pop(2*(j-1)+2,z);
                        n_pop2(1,z) = n_pop(2*(j-1)+1,z);
                    end
                end
                n_pop(2*(j-1)+1,:) = n_pop1;
                n_pop(2*(j-1)+2,:) = n_pop2;
            end
        end
    end
    % Aplica��o de muta��o n�o-uniforme onde for escolhido
    n_mut = round(tam_pop*3*pm);
    bits_mutados = 1 + round((tam_pop*3-1)*rand(n_mut,1));
    for i=1:n_mut,
        if rem(bits_mutados(i),3) == 0,
            linha = fix(bits_mutados(i)/3);
            coluna = 3;
        else
            linha = fix(bits_mutados(i)/3)+1;
            coluna = rem(bits_mutados(i),3);
        end
        if rand(1,1) <= 0.5,
            delta = mut_nunif(k,v_sup-n_pop(linha,coluna),n_ger);
            v_delta = [v_delta;delta];
            n_pop(linha,coluna) = n_pop(linha,coluna) + delta;
        else
            delta = mut_nunif(k,n_pop(linha,coluna)-v_inf,n_ger);
            v_delta = [v_delta;delta];
            n_pop(linha,coluna) = n_pop(linha,coluna) - delta;
        end
    end
    % Avalia��o da nova popula��o
    for i=1:tam_pop,
        kp = n_pop(i,1);
        kd = n_pop(i,2);
        ki = n_pop(i,3);
        S = stepinfo_EA072(G_MF(kp,kd,ki));
        [Gm,Pm,Wcg,Wcp] = margin(G_MF(kp,kd,ki));
        t1 = 0.0;t2 = 0.0;t3 = 0.0;
        if isnan(S.SettlingTime) || isnan(S.RiseTime) || isnan(S.Overshoot),
            fitness(i,1) = 0;
        else
            if S.SettlingTime > 0.5,
                t1 = S.SettlingTime - 0.5;
            end
            if S.RiseTime > 0.04,
                t2 = S.RiseTime - 0.04;
            end
            if S.Overshoot > 0.15,
                t3 = S.Overshoot - 0.15;
            end;
            fitness(i,1) = 1/(t1+(0.5/0.04)*t2+t3+1);
        end
    end
    % Preserva��o do melhor indiv�duo da gera��o anterior, se melhor que o melhor da nova gera��o
    melhor_fitness1 = melhor_fitness;
    melhor_ind1 = melhor_ind;
    [melhor_fitness,melhor_ind] = max(fitness);
    if melhor_fitness1 > melhor_fitness,
        [pior_fitness,pior_ind] = min(fitness);
        n_pop(pior_ind,:) = pop(melhor_ind1,:);
        fitness(pior_ind,1) = melhor_fitness1;
        melhor_fitness = melhor_fitness1;
        melhor_ind = pior_ind;
    end
    pop = n_pop;
    v_melhor_fitness = [v_melhor_fitness;melhor_fitness];
    v_fitness_medio = [v_fitness_medio;mean(fitness)];
    v_melhor_ind = [v_melhor_ind;pop(melhor_ind,:)];
    kp = pop(melhor_ind,1);
    kd = pop(melhor_ind,2);
    ki = pop(melhor_ind,3);
    f1 = figure(1);step(G_MF(kp,kd,ki),T);drawnow;
    f2 = figure(2);
    subplot(3,1,1);hist(pop(:,1),B);
    title(sprintf('Distribuicao do ganho kp na populacao - Geracao %d',k));drawnow;
    subplot(3,1,2);hist(pop(:,2),B);
    title(sprintf('Distribuicao do ganho kd na populacao - Geracao %d',k));drawnow;
    subplot(3,1,3);hist(pop(:,3),B);
    title(sprintf('Distribuicao do ganho ki na populacao - Geracao %d',k));drawnow;
    S = stepinfo_EA072(G_MF(kp,kd,ki));
    [Gm,Pm,Wcg,Wcp] = margin(G_MF(kp,kd,ki));
    disp(sprintf('Geracao %d: T_sub = %g | T_acom = %g | Sobrs = %g | Margfase = %g',k,S.RiseTime,S.SettlingTime,S.Overshoot,Pm));
    disp(sprintf('Geracao %d: Os melhores valores encontrados foram: k_p = %g; k_d = %g; k_i = %g',k,kp,kd,ki));
    disp(sprintf('Geracao %d: Fitness deste melhor individuo = %g',k,melhor_fitness));
end
f3 = figure(3);plot(v_melhor_fitness,'k');hold on;plot(v_fitness_medio,'r');hold off;
title('Melhor fitness (preto) e fitness m�dio (vermelho) da populacao ao longo das geracoes');
f4 = figure(4);plot(v_melhor_ind(:,1),'k');hold on;plot(v_melhor_ind(:,2),'r');plot(v_melhor_ind(:,3),'b');hold off;
title('Evolucao dos ganhos do controlador PID: k_p (preto) | k_d (vermelho) | k_i (azul)');
xlabel('N�mero de geracoes');
f5 = figure(5);plot(v_delta);
title('Intensidade da mutacao nao-uniforme ao longo das geracoes');

saveas(f1, "Figure2-1.png");
saveas(f2, "Figure2-2.png");
saveas(f3, "Figure2-3.png");
saveas(f4, "Figure2-4.png");
saveas(f5, "Figure2-5.png");
